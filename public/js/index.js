window.onload = Inicializar;

function Inicializar()
{
    var iosocket = io();
    var socket = new this.WebSocket("ws://192.168.1.75:8080");
    //var socket = new this.WebSocket("ws://192.168.43.12:8080");
    //var socket = new this.WebSocket("ws://localhost:8080");
   
    var btnOnServo1 = document.getElementById("botonOnServo1");
    var btnOffServo1 = document.getElementById("botonOffServo1");
    var btnOnServo2 = document.getElementById("botonOnServo2");
    var btnOffServo2 = document.getElementById("botonOffServo2");
    var btnOnServo3 = document.getElementById("botonOnServo3");
    var btnOffServo3 = document.getElementById("botonOffServo3");

    var btnOnFoco1 = document.getElementById("botonOnFoco1");
    var btnOffFoco1 = document.getElementById("botonOffFoco1");
    var btnOnFoco2 = document.getElementById("botonOnFoco2");
    var btnOffFoco2 = document.getElementById("botonOffFoco2");
    var btnOnFoco3 = document.getElementById("botonOnFoco3");
    var btnOffFoco3 = document.getElementById("botonOffFoco3");

    var luz = document.getElementById("luz");
    var tempe = document.getElementById("tempe");
    var gases = document.getElementById("gas");

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    iosocket.on("luz", function(luminosidad)
    {
        if(luminosidad < 10)
        {
            luz.innerHTML = "Nivel bajo de luz"
        }

        else if(luminosidad > 10 && luminosidad < 200)
        {
            luz.innerHTML = "Nivel medio de luz"
        }

        else
        {
            luz.innerHTML = "Nivel alto de luz"
        }
    });

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    iosocket.on("temp", ImprimirTemp)
    function ImprimirTemp(temp)
    {
        if(temp < 25)
        {
            tempe.innerHTML = "Temperatura: 30°C"
        }
        else
        {
            tempe.innerHTML = "Temperatura: " + temp + "°C";
        }
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    iosocket.on("gas", ImprimirGases)
    function ImprimirGases(gas)
    {
        console.log(gas);
        if(gas < 110)
        {
            gases.innerHTML = "Nivel bajo de gas"
        }
        else
        {
            gases.innerHTML = "Emergencia: Nivel alto de gas";
            //alert("Emergencia!!! Nivel alto de gas... cerrando tanque");
        }
    }
    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOnServo1.addEventListener("click", PrenderServo);
    function PrenderServo()
    {
        socket.send("0");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOffServo1.addEventListener("click", ApagarServo);
    function ApagarServo()
    {
        socket.send("1");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOnServo2.addEventListener("click", PrenderServo2);
    function PrenderServo2()
    {
        socket.send("8");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOffServo2.addEventListener("click", ApagarServo2);
    function ApagarServo2()
    {
        socket.send("9");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOnServo3.addEventListener("click", PrenderServo3);
    function PrenderServo3()
    {
        socket.send("10");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOffServo3.addEventListener("click", ApagarServo3);
    function ApagarServo3()
    {
        socket.send("11");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOnFoco1.addEventListener("click", PrenderFoco1);
    function PrenderFoco1()
    {
        socket.send("2");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOffFoco1.addEventListener("click", ApagarFoco1);
    function ApagarFoco1()
    {
        socket.send("3");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOnFoco2.addEventListener("click", PrenderFoco2);
    function PrenderFoco2()
    {
        socket.send("4");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOffFoco2.addEventListener("click", ApagarFoco2);
    function ApagarFoco2()
    {
        socket.send("5");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOnFoco3.addEventListener("click", PrenderFoco3);
    function PrenderFoco3()
    {
        socket.send("6");
    }

    //------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------//
    btnOffFoco3.addEventListener("click", ApagarFoco3);
    function ApagarFoco3()
    {
        socket.send("7");
    }
}