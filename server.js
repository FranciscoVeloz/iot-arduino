var express = require("express");
var aplicacion = express();

var http = require("http");
var servidor = http.createServer(aplicacion);

var five = require("johnny-five");
var circuito = new five.Board();

var WebSocket = require("ws");
var wSocket = new WebSocket.Server({port: 8080});

var socketIO = require("socket.io");
var io = socketIO.listen(servidor);

servidor.listen(3000, function()
{
    console.log("Conectado");
});

var celda, temperatura, gas;

aplicacion.use('/Inicio', express.static('public'));

circuito.on("ready", Principal);

function Principal()
{
    //Entradas
    var configuracion_luz = {pin: "A5", freq: 50}
    celda = new five.Sensor(configuracion_luz);

    temperatura = new five.Thermometer({
        controller: "LM35",
        pin: "A0"
    });

    gas = new five.Sensor("A2");

    //Salidas
    var motor1 = new five.Servo(8);
    var motor2 = new five.Servo(9);
    var motor3 = new five.Servo(10);

    var rele1 = new five.Relay(4);

    var rele2 = new five.Relay(5);
    var rele3 = new five.Relay(6);
    var rele4 = new five.Relay(7);

    rele1.off();
    rele2.off();
    rele3.off();
    rele4.off();

    motor1.to(70);
    motor2.to(90);
    motor3.to(0);


   wSocket.on("connection", Conectado);

    function Conectado(ws, req)
    {
        console.log("Conectado...");
        ws.on("message", LecturaMensaje);

        function LecturaMensaje(data)
        {
            var dato = parseInt(data);
            switch (dato)
            {
                //Puerta
                case 0:
                    motor1.to(0);
                    break;

                case 1:
                    motor1.to(70);
                    break;

                //Foco1
                case 2:
                    rele4.on();
                    break;
                
                case 3:
                    rele4.off();
                    break;

                //Foco2
                case 4:
                    rele3.on();
                    break;
                
                case 5:
                    rele3.off();
                    break;

                //Foco3
                case 6:
                    rele2.on();
                    break;
                
                case 7:
                    rele2.off();
                    break;

                //Ventana1
                case 8:
                    motor2.to(0);
                    break;

                case 9:
                    motor2.to(90);
                    break;

                //Ventana2
                case 10:
                    motor3.to(90);
                    break;

                case 11:
                    motor3.to(0);
                    break;
            }
        }
    }
    
    ImprimirTodo();
}

function ImprimirTodo()
{
    ImprimirLed();
    LecturaTemp();
    ImprimirGas();
    setTimeout(ImprimirTodo, 1000);
}

function ImprimirLed()
{
    io.emit("luz", parseInt(celda.value));
}

function LecturaTemp()
{
    const {celsius} = temperatura;
    io.emit("temp", parseInt(celsius));
}

function ImprimirGas()
{
    io.emit("gas", parseInt(gas.value));
}