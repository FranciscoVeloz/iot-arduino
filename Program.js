var johnny_five = require("johnny-five"); //Referenciamos la libreria de Johnny-five
var circuito = johnny_five.Board(); //Creamos un objeto de la libreria llamado circuito
var celda, temperatura, temp;

circuito.on("ready", Prender); //Cuando el circuito este listo, ejecutamos la funcion "prender"

function Prender() 
{ 
    celda = new johnny_five.Sensor({ pin: "A5", freq: 50 });

    /*
    temperatura = new johnny_five.Thermometer({
        controller: "LM35",
        pin: "A0"
    });
    */

    temp = new johnny_five.Sensor({pin: "A0", freq: 50});

    ImprimirTodo();
}

function ImprimirTodo() {
    temperatura = (temp.value * 500) / 1024;
    ImprimirLed();
    //LecturaTemp();
    Temper();
    setTimeout(ImprimirTodo, 1000);
}

function ImprimirLed() {
    console.log("Luz: " + celda.value);
}

function Temper()
{
    console.log("Temperatura: " + temperatura);
}

/*
function LecturaTemp() {
    const { celsius } = temperatura;
    console.log("Temperatura: " + celsius + "°");
}
*/